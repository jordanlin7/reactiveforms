import { Component, OnInit } from '@angular/core';
import { FormArray, FormControl, FormGroup, Validators } from '@angular/forms';
import { ResolveEnd } from '@angular/router';
import { Observable } from 'rxjs';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent implements OnInit{
  genders = ['male', 'female'];

  signupForm: FormGroup
  forbiddenUsername = ['Chris', 'Anna'];

  ngOnInit(): void {
    //
    this.signupForm = new FormGroup({
      'userData' : new FormGroup({
        'username' : new FormControl(null, [Validators.required, this.forbiddenNames.bind(this)]), // 這邊要bind(this)，因為在使用這個validator的是angular，那個時候的this並不一定是我們這個.ts的this)
        'email' : new FormControl(null, [Validators.required, Validators.email], this.forbiddenEmails)
      }),
      'gender': new FormControl('male'),
      'hobbies' : new FormArray([])
    });

    // this.signupForm.valueChanges.subscribe(value=>
    //   console.log(value)
    // )

    this.signupForm.statusChanges.subscribe(status=>
      console.log(status)
    )

    // setvalue
    this.signupForm.setValue({
      'userData' : {
        'username' : 'xxx',
        'email' : 'xxx@gmail.com'
      },
      'gender' : 'male',
      'hobbies' : []

    })

    // patch value, 更新data
    this.signupForm.patchValue({
      'userData' : {
        'username' : 'hahaha'
      }
    })


  }


  onSubmit() {
    console.log(this.signupForm);
    this.signupForm.reset({
      'gender': 'male'
    });

  }

  onAddHobby() {
    const control = new FormControl(null, Validators.required);
    (<FormArray>this.signupForm.get('hobbies')).push(control)

  }

  get controls() {
    return (this.signupForm.get('hobbies') as FormArray).controls;
  }


  forbiddenNames(control : FormControl) : {[s:string] : boolean} {
    if(this.forbiddenUsername.indexOf(control.value) !== -1) {
      return {'nameIsForbidden' : true};
    }

    // 這邊要回傳null，或是乾脆不回傳，不能傳回false
    return null;
  }

  forbiddenEmails(control: FormControl) : Promise<any> | Observable<any> {
    const promise = new Promise<any>((resolve, reject)=> {
      setTimeout(()=> {
        if(control.value === 'test@test.com') {
          resolve({'emailIsForbidden' : true});
        }
        else {
          resolve(null);
        }
      }, 1500)
    })

    return promise;
  }


}


